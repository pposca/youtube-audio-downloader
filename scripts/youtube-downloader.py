# qpy:console
import os
import subprocess
import pafy

from colorama import init, Fore, Style

PATH = '/storage/sdcard1/Music/'

init(autoreset=True)
url = input('{}Video url: '.format(Fore.CYAN))
video = pafy.new(url)
audio = video.getbestaudio()

filename = '{} - {} - {}.{}'.format(
    audio.title.replace('|', '-'),
    video.videoid,
    '192k',
    audio.extension)

print('{}Downloading...'.format(Fore.CYAN))
filepath = os.path.join(PATH, filename)
audio.download(filepath=filepath)

print('\n{}Converting...'.format(Fore.CYAN))
mp3_filename = '{}.mp3'.format(filename[:-4])
subprocess.call([
    "ffmpeg", "-i",
    os.path.join(PATH, filename),
    "-acodec", "libmp3lame", "-ab", "192k",
    os.path.join(PATH, mp3_filename)
])

print('{}Deleting temp file:\n{}'.format(Fore.CYAN, filename))
os.remove(filepath)

print('{}{}{}'.format(Style.BRIGHT, Fore.GREEN, mp3_filename))