$(document).ready(function() {

    function get_audio(url) {

        $('#form button#go').prop("disabled", true);
        $('#form input#url').prop("disabled", true);

        $.ajax({
            type:       "POST",
            url:        'ajax/get-audio/',
            data:       {url:url},
            timeout:    10000
        })
        .done(function(response) {
            var result = $.parseJSON(response);
            get_status(result.task_id);
        })
        .fail(function(jqXHR, textStatus) {
            ajax_error(textStatus);
        })
    };

    function get_status(task_id) {

        $.ajax({
            type:       "GET",
            url:        'ajax/get-status/' + task_id,
            timeout:    5000
        })
        .done(function(response) {

            $('#progress-info').show();
            update_progress(0);

            var result = $.parseJSON(response);

            $('#audio-title').html(result.text);
            if (result.state == "PENDING" || result.state == "STARTED") {
                setTimeout(function() { get_status(task_id); }, 2000);
            }
            else if (result.state == "DOWNLOADING") {
                update_progress(result);
                setTimeout(function() { get_status(task_id); }, 2000);
            }
            else if (result.state == "SUCCESS") {
                update_progress(100);
                download_button(result.link);
                $('#form button#go').prop("disabled", false);
                $('#form input#url').prop("disabled", false);
            }
            else {
                ajax_error("Sorry, try with other video");
            }
        })
        .fail(function(jqXHR, textStatus) {
            ajax_error(textStatus);
        })
    }

    function update_progress(result) {
        var bar = $('.progress-bar');
        var eta = $('#eta');
        var rate = $('#rate');
        var progress = 0
        if (result == 0 || result == 100) {
            progress = result;
            eta.text("0:00:00 remaining");
            rate.text("0 KB/s");
            if (result == 0)
                bar.removeClass("progress-bar-success");
            else {
                bar.addClass("progress-bar-success");
                bar.removeClass("active");
            }
        }
        else {
            bar.addClass("active");
            eta.text(result.eta + " remaining");
            rate.text(result.rate + " KB/s");
            progress = result.ratio;
        }
        bar.css('width', progress + '%');
        bar.attr('aria-valuenow', progress);
        bar.text(progress + '%');
    }

    function ajax_error(error_msg) {
        msg_alert("ERROR " + error_msg);
        $('#form button#go').prop("disabled", false);
        $('#form input#url').prop("disabled", false);
    }

    function msg_alert(text) {

        $('<p>').addClass('msg-alert').addClass('bg-danger').text(text).prependTo($('#msg-container'));
    };

    function download_button(link) {
        $('<a>').addClass('btn btn-primary btn-lg active')
            .attr('role', 'button')
            .attr('href', link)
            .attr('id', 'download-button')
            .attr('download', '')
            .text("DOWNLOAD")
            .prependTo($('#download-button-container')
            .show());
    }

    $("#form").submit(function(event) {

        event.preventDefault();

        $('#progress-info').show();
        $('.msg-alert').remove();
        $('#download-button').remove();

        url = $.trim($('#url').val());

        if (url) {
            get_audio(url);
        }
    });

});
