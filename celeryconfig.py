from datetime import timedelta

BROKER_URL = 'amqp://guest@localhost//'
CELERY_RESULT_BACKEND = 'amqp://guest@localhost//'

# CELERY_TIMEZONE = 'UTC',
# CELERY_ENABLE_UTC = True,

CELERYD_TASK_TIME_LIMIT = 900  # 15 min

CELERYBEAT_SCHEDULE = {
    # Executes every Monday morning at 7:30 A.M
    'clean-audios-every-hour': {
        'task': 'tasks.clean_audios',
        'schedule': timedelta(hours=1),
    },
}
