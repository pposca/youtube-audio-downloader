import pafy
import os
from peewee import *
from youtube_audio_downloader import celery
from models import Audio

AUDIO_PATH = os.path.abspath(os.path.dirname(__file__)) + '/static/audio/'



@celery.task(bind=True)
def download_audio(self, video_url):
    self.update_state(state='STARTED')

    video = pafy.new(video_url)
    best_audio = video.getbestaudio()
    audio = Audio(
        video_url=video_url,
        title=best_audio.title,
        filename=best_audio.filename,
        bitrate=best_audio.bitrate,
        size=best_audio.get_filesize(),
        videoid=video.videoid,
        extension=best_audio.extension,
        itag=best_audio.itag,
        unique_filename='{} - {} - {}.{}'.format(best_audio.title,
                        video.videoid, best_audio.bitrate, best_audio.extension)
    )

    os.

    try:
        db_audio = Audio.get(Audio.unique_filename == audio.unique_filename)
        return db_audio.as_dict()

    except DoesNotExist:
        def update_status(total, recvd, ratio, rate, eta, audio=audio):
            meta = audio.as_dict()
            meta.update({
                'ratio': round(ratio*100),
                'eta': round(eta),
                'rate': round(rate, 2),
            })
            self.update_state(state='DOWNLOADING', meta=meta)

        filepath = AUDIO_PATH + audio.unique_filename
        best_audio.download(filepath=filepath, callback=update_status)
        audio.save()

        return audio.as_dict()


@celery.task()
def clean_audios():
    print('MOLA')

