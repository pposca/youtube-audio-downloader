import datetime
from peewee import *

db = SqliteDatabase('database.db')


class BaseModel(Model):
    class Meta:
        database = db


class Audio(BaseModel):
    created_date = DateTimeField(default=datetime.datetime.now)
    video_url = CharField()
    title = CharField()
    filename = CharField()
    bitrate = CharField()
    size = BigIntegerField()
    videoid = CharField()
    extension = CharField()
    itag = CharField()
    signature = CharField(default='')
    unique_filename = CharField(unique=True)

    def as_dict(self):
        return {
            'created_date': self.created_date,
            'video_url': self.video_url,
            'title': self.title,
            'filename': self.filename,
            'bitrate': self.bitrate,
            'size': self.size,
            'videoid': self.videoid,
            'extension': self.extension,
            'itag': self.itag,
            'signature': self.signature,
            'unique_filename': self.unique_filename
        }

    def update_signature(self):
        import hashlib
        signature = hashlib.md5()
        for key, value in self.as_dict().items():
            if key not in ['created_date']:
                signature.update(str(value).encode('utf-8'))

        self.signature = signature.hexdigest()

# Connet to database and create tables if needed
db.connect()
db.create_tables([Audio], safe=True)
