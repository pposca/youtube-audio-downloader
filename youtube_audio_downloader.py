import json
import models
from celery import Celery
from flask import Flask, render_template, request, url_for
from flask_admin import Admin
from flask_admin.contrib.peewee import ModelView

BASE_URL = '/youtube-audio-downloader'
app = Flask(__name__, static_path=BASE_URL + '/static')

admin = Admin(
    app,
    name='youtube-audio-downloader',
    template_mode='bootstrap3',
    url=BASE_URL + '/admin'
)
admin.add_view(ModelView(models.Audio))

celery = Celery(app.name)
celery.config_from_object('celeryconfig')

import tasks


@app.route(BASE_URL + '/')
def index():
    return render_template('index.html')


@app.route(BASE_URL + '/ajax/get-audio/', methods=['POST'])
def get_audio():
    task = tasks.download_audio.delay(request.form['url'])

    return json.dumps({'task_id': task.id})


@app.route(BASE_URL + '/ajax/get-status/<task_id>')
def get_status(task_id):
    task = tasks.download_audio.AsyncResult(task_id)
    if task.state == 'PENDING':
        response = {
            'state': task.state,
            'text': 'Task queued - Waiting to be started...'
        }
    elif task.state == 'STARTED':
        response = {
            'state': task.state,
            'text': 'Task started - Retrieving audio information...'
        }
    elif task.state == 'DOWNLOADING':
        response = {
            'state': task.state,
            'text': "<b>{}</b><br>{} - {} - {}".format(
                task.info.get('title', 'Unknown'),
                task.info.get('bitrate', ''),
                task.info.get('extension', ''),
                human_size(task.info.get('size', 0))
            ),
            'ratio': task.info.get('ratio', 0),
            'eta': human_time(task.info.get('eta', 0)),
            'rate': task.info.get('rate', 0),
        }
    elif task.state == 'SUCCESS':
        response = {
            'state': task.state,
            'text': "<b>{}</b><br>{} - {} - {}".format(
                task.info.get('title', 'Unknown'),
                task.info.get('bitrate', ''),
                task.info.get('extension', ''),
                human_size(task.info.get('size', 0))
            ),
            'link': url_for('static',
                            filename='audio/' + task.info['unique_filename'])
        }
    else:
        # something went wrong in the background job
        response = {
            'state': task.state,
            'status': str(task.info),  # this is the exception raised
        }
    return json.dumps(response)


def human_size(num_bytes, suffix='B'):
    for unit in ['', 'K', 'M', 'G', 'T', 'P', 'E', 'Z']:
        if abs(num_bytes) < 1024.0:
            return "%3.1f %s%s" % (num_bytes, unit, suffix)
        num_bytes /= 1024.0
    return "%.1f %s%s" % (num_bytes, 'Yi', suffix)


def human_time(secs):
    import datetime
    return str(datetime.timedelta(seconds=secs))


if __name__ == '__main__':
    app.debug = True
    app.run(host='192.168.1.105')
